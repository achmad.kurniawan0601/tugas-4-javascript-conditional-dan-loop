// soal 1

// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

var nilai = 75;

if (nilai >= 85) {
    console.log("indeksnya A");
}else if(nilai >= 75 && nilai < 85){
    console.log("indeksnya B");
}else if(nilai >= 65 && nilai < 75){
    console.log("indeksnya C");
}else if(nilai >= 55 && nilai < 65){
    console.log("indeksnya D");
}else{
    console.log("indeksnya E");
}

console.log("\n");
// document.write("<br><br>")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// soal 2

// buatlah variabel seperti di bawah ini

// var tanggal = 22;
// var bulan = 7;
// var tahun = 2020;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, 
// lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 
// (isi di sesuaikan dengan tanggal lahir masing-masing)

var tanggal = 22;
var bulan = 7;
var tahun =  2020;

switch (bulan) {
    case 1: {console.log('22 Januari 2020'); break; }
    case 2: {console.log('22 Februari 2020'); break; }
    case 3: {console.log('22 Maret 2020'); break; }
    case 4: {console.log('22 April 2020'); break; }
    case 5: {console.log('22 Mei 2020'); break; }
    case 6: {console.log('22 Juni 2020'); break; }
    case 7: {console.log('22 Juli 2020'); break; }
    case 8: {console.log('22 Agustus 2020'); break; }
    case 9: {console.log('22 September 2020'); break; }
    case 10: {console.log('22 Oktober 2020'); break; }
    case 11: {console.log('22 November 2020'); break; }
    case 12: {console.log('22 Desember 2020'); break; }
    default: {console.log('Silahkan masukkan bulan yang sesuai'); }}

console.log("\n\n");
// soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output untuk n=3 :

// #
// ##
// ###
// Output untuk n=7 :

// #
// ##
// ###
// ####
// #####
// ######
// #######

var n;
var l;

console.log("Output untuk n=3");
for(n=1;  n<4; n++){
    for(l=0; l<n; l++){
        console.log("#");
    }
    console.log("");
}
// console.log(l);

console.log("");
console.log("Output untuk n=7");
for(n = 1; n < 8; n++){
    for(l = 0; l < n; l++){
        console.log("#");
    }
    console.log("");
}

console.log("\n");
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// soal 4
// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// contoh :

// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

// Output untuk m = 5

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript

// Output untuk m = 7

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming

var program = "I Love Programming";
var js = "I Love Javascript";
var vue = "I Love VueJS";

console.log("Output untuk m=3");
for(a = 1; a < 4; a++){
    if (a % 3 == 1){
        console.log(a + " " + program);
    }else if(a % 3 == 2){
        console.log(a + " " + js);
    }else{
        console.log(a + " " + vue);
        console.log("=======")
    }
}

console.log("\n");
console.log("Output untuk m=5");
for(a = 1; a < 6; a++){
    if (a % 3 == 1){
        console.log(a + " " + program);
    }else if(a % 3 == 2){
        console.log(a + " " + js);
    }else{
        console.log(a + " " + vue);
        console.log("=======")
    }
}

console.log("\n");
console.log("Output untuk m=7");
for(a = 1; a < 8; a++){
    if (a % 3 == 1){
        console.log(a + " " + program);
    }else if(a % 3 == 2){
        console.log(a + " " + js);
    }else{
        console.log(a + " " + vue);
        console.log("=======")
    }
}

console.log("\n");
console.log("Output untuk m=10");
for(a = 1; a < 11; a++){
    if (a % 3 == 1){
        console.log(a + " " + program);
    }else if(a % 3 == 2){
        console.log(a + " " + js);
    }else{
        console.log(a + " " + vue);
        console.log("=======")
    }
}